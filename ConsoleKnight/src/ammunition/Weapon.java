package ammunition;

public class Weapon extends Ammunition {
	private String nameWeapon;
	private int damage;

	public Weapon(String name, int damage, int level, int weight, int cost) {
		super(cost, weight, level);
		this.nameWeapon = name;
		this.damage = damage;
	}

	public int getDamage() {
		return damage;
	}
	public void setDamage(int damage) {
		this.damage=damage;
	}
	public String getNameWeapon() {
		return nameWeapon;
	}

	public void setNameWeapon(String nameWeapon) {
		this.nameWeapon = nameWeapon;
	}
}
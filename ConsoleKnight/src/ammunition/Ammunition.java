package ammunition;

public class Ammunition {
	private int cost;
	private int weight;
	private int level;

	public Ammunition(int cost, int weight, int level) {
		this.cost = cost;
		this.weight = weight;
		this.level = level;
	}

	public int getCost() {
		return cost;
	}

	public int getWeight() {
		return weight;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getLevel()
	{
		return this.level;
	}
	// interface upgrade()
}

package ammunition;

public class Armor extends Ammunition {
	private String nameHelmet;
	private int strenght;

	public Armor(String name, int level, int strenght, int weight, int cost) {
		super(weight, cost, level);
		this.nameHelmet = name;
		this.strenght = strenght;
	}

	public int TakeDamage(int damage) {
		int damageTaken;
		damageTaken = strenght - damage;
		strenght=(int)(strenght-damage*0.2);
		return damageTaken;
	}

	public String getName() {
		return this.nameHelmet;
	}
	public int getStrenght()
	{
		return this.strenght;
	}
	public void setStrenght(int i) {
		this.strenght=i;
	}
	public void setName(String name) {
		this.nameHelmet=name;
	}
	
}
